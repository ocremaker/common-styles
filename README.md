# Common Styles

---

A list of style mixins that define common styles to be used by both [skins](https://framagit.org/ocremaker/skins) and
[IFREAD](https://framagit.org/ocremaker/Ifread).

